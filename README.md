## Node.js Discord Server

### What is this?

This is a discord server available for the general public to use as a resource and tool when developing with Node.js and NPM.

### Rules

**General Rules** 
* No spam.
* No advertisements. 
* No harassment. 
* No racist or NSFW content. 
* No links to illegal content. 
* Respect copyright licenses. 
* Don't upload illegal files. 

**Development Rules** 
* Please post questions and discussions to the correct channels.
* Stick to the channel rules! Read the channel description so you don't get warned even if you just wanted to be funny, use offtopic channels for the fun! :) 
* If you have questions, please provide your sourcecode or links to codepen.
* Please be friendly and don't spam for feedback. 
* Behave like an adult. 
* Please don't ask everyone for them to code your stuff. The community is about learning and discussion, not about getting your code written by someone else. 

 _Breaking the rules will lead to a warning or ban._


### Joining

[You can join the Discord-Server here](https://discord.gg/WT8ZcMr)